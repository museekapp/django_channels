#!/usr/bin/env python
#  -*- coding: utf-8 -*-
#  -*- See PEP 0263 for encoding definition
#
# urls.py
#
from rest_framework.routers import DefaultRouter
from .views import ThingViewSet

hello_router = DefaultRouter()
hello_router.register(r'things', ThingViewSet)
