#!/usr/bin/env python
#  -*- coding: utf-8 -*-
#  -*- See PEP 0263 for encoding definition
#
# serializers.py
#
from rest_framework import serializers

from .models import Thing


class ThingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Thing
        fields = ('name', 'title')

    def create(self, validated_data):
        print('THINGSerializer.create')
        return Thing.objects.create(**validated_data)

    def update(self, instance, validated_data):
        print('THINGSerializer.update')
        instance.name = validated_data.get('name', instance.email)
        instance.title = validated_data.get('title', instance.created)
        instance.save()
        return instance
