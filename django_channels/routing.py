#!/usr/bin/env python
#  -*- coding: utf-8 -*-
#  -*- See PEP 0263 for encoding definition
#
# routing.py
#

from channels.generic.websockets import WebsocketDemultiplexer
from channels.routing import route, route_class
from hello.bindings import ThingBinding
# from hello.consumers import ws_connect, ws_message, ws_disconnect


class APIDemultiplexer(WebsocketDemultiplexer):
    consumers = {
        'things': ThingBinding.consumer
    }

channel_routing = [
    route_class(APIDemultiplexer),
    # route("websocket.connect", ws_connect),
    # route("websocket.receive", ws_message),
    # route("websocket.disconnect", ws_disconnect),
]
