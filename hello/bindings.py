#!/usr/bin/env python
#  -*- coding: utf-8 -*-
#  -*- See PEP 0263 for encoding definition
#
# bindings.py
#

from channels_api.bindings import ResourceBinding
from .models import Thing
from .serializers import ThingSerializer
from channels.binding.base import CREATE, UPDATE, DELETE
from pprint import pprint as pp
from channels import Group
from rest_framework.exceptions import ValidationError
from django.db.models.signals import post_delete, post_save, pre_delete, pre_save

import sys
sys.path.append('/opt/django_channels/pycharm-debug-py3k.egg')
import pydevd
pydevd.settrace('cloudmb1.ddns.net', port=30020, stdoutToServer=True, stderrToServer=True, suspend=False)


class ThingBinding(ResourceBinding):
    # model = Thing
    model = Thing
    stream = 'things'
    serializer_class = ThingSerializer
    queryset = Thing.objects.all()

    # def __init__(self, *args, **kwargs):
    #     raise Exception('sdf-linsdfi nsi nfINIT')

    @classmethod
    def register(cls):
        """
        Resolves models.
        """
        print('REGISTER')
        print('CLASS NAME: %s' % cls.__name__)
        print('REGISTERED MODELS: %s' % str([m for m in cls.get_registered_models()]))
        # Connect signals
        for model in cls.get_registered_models():
            pre_save.connect(cls.pre_save_receiver, sender=model)
            post_save.connect(cls.post_save_receiver, sender=model)
            pre_delete.connect(cls.pre_delete_receiver, sender=model)
            post_delete.connect(cls.post_delete_receiver, sender=model)

    def subscribe(self, pk, data, **kwargs):
        print('SUBSCRIBE')
        if 'action' not in data:
            raise ValidationError('action required')
        group_name = self._group_name(data['action'], id=pk)
        print('GROUP NAME: %s' % group_name)
        print('message.reply_channel: %s' % self.message.reply_channel)
        Group(group_name).add(self.message.reply_channel)
        return {}, 200

    @classmethod
    def group_names(cls, instance, action):
        print('GROUP NAMES')
        self = cls()
        groups = [self._group_name(action)]
        if instance.id:
            groups.append(self._group_name(action, id=instance.id))
        return groups

    @classmethod
    def post_save_receiver(cls, instance, created, **kwargs):
        print('POST SAVE RECIEVER')
        print('cls.__dict__')
        pp(cls.__dict__)
        print('instance.__dict__')
        pp(instance.__dict__)
        print('created: %s' % created)
        print('kwargs')
        pp(kwargs)
        print('cls.post_change_receiver: %s' % cls.post_change_receiver)
        cls.post_change_receiver(instance, CREATE if created else UPDATE, **kwargs)

    @classmethod
    def post_change_receiver(cls, instance, action, **kwargs):
        """
        Triggers the binding to possibly send to its group.
        """
        # raise Exception('SADFASDFAS DFSA ASDF SADF SAD F ASDF SADFSDA FSA FSDAF SADF SADF SAD')
        print('POST CHANGE RECEIVER')
        old_group_names = instance._binding_group_names[cls]
        if action == DELETE:
            new_group_names = set()
        else:
            new_group_names = set(cls.group_names(instance, action))

        print('NEW GROUP NAMES')
        pp(new_group_names)

        # if post delete, new_group_names should be []
        self = cls()
        self.instance = instance

        print('SELF')
        pp(self.__dict__, indent=2)

        print('old_group_names & new_group_names')
        pp(old_group_names & new_group_names, indent=2)

        print('KWARGS')
        pp(kwargs)

        # Django DDP had used the ordering of DELETE, UPDATE then CREATE for good reasons.
        self.send_messages(instance, old_group_names - new_group_names, DELETE, **kwargs)
        self.send_messages(instance, old_group_names & new_group_names, UPDATE, **kwargs)
        self.send_messages(instance, new_group_names - old_group_names, CREATE, **kwargs)

