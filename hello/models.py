from django.db import models


class Thing(models.Model):
    name = models.CharField(max_length=100, null=True, blank=True)
    title = models.CharField(max_length=100, null=True, blank=True)

    # def save(self, *args, **kwargs):
    #     print('THING SAVE')
    #     super(Thing, self).save(*args, **kwargs)
